// Separation of concerns
/*
	Why do we have to..?
	1. Better code readability
	2. Improved Scalability
	3. Better code maintainability

	What do we separate..?
	1. Models
	2. Controllers
	3. Routes
*/

// Mongoose has an internal feature that if there is no b242_s36 or something, it automatically creates a database that you have specified in the string provided..

// __v is a default version for mongoDB and it represents the iterations or number of times it changed..
 

// Dependencies
const express = require('express');
const mongoose = require('mongoose');

// routes
const taskRoute = require("./routes/taskRoute.js");

// server
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database connection
mongoose.connect("mongodb+srv://Baggam_Rakshan_Tej:x3dOSxedvkf9xy91@cluster0.1i9omqc.mongodb.net/b242-s36?retryWrites=true&w=majority", 
{
	useNewUrlParser : true,
	useUnifiedTopology : true
}
);

// add the task route
app.use('/tasks', taskRoute);

// Server listening
app.listen(port, () => console.log(`Now listening to the port ${port}`));