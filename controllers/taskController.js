// allow us to use the contents of the "tasks.js" file in the models folder
const Task = require('../models/task.js');
// We are going to build the models later

// get all tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
};

// create a new task
module.exports.createTask = (requestBody) => {
	// Create a new task object based on the mongoose model "Task"
	let newTask = new Task({
		// sets the "name" property with the value received from the client/Postman
		name: requestBody.name
	})
	return newTask.save().then((task, error)=>{
		if(error){
			console.log(error);
			return false;
		}
		else {
			return task;
		}
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
		if(error){
			console.log(error);
			return false;
		}
		else{
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}
		else{
			result.name = newContent.name;
			return result.save().then((updatedTask, saveErr) => {
				if(saveErr){
					console.log(saveErr);
					return false;
				}
				else{
					return updatedTask;
				}
			})
		}
	})
}

// update a task
/*
Business Logic
	1. get the task with the id using findById
	2. replace the task's name returned from the db with the "name" property from the requestBody
	3. Save the task
*/

