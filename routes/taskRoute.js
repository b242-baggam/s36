// contains all endpoints of our application
const express = require("express");
// creates Router instance that functions as a middleware and routing system
// allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

// import the controller for tasks
// allows us to use the functions inside/defined in 'taskController.js'
const taskController = require("../controllers/taskController");

// [Section] Routes
/*
	- routes are responsible for defining URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed
	- app.post("", (req, res)=> {}) this is responsible for the routers
*/
/*
	Getting all tasks
		this route expects to receive a GET request at the URL "/tasks"
*/
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// route to create a new task
router.post('/', (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// route to delete a task
router.delete('/:id', (req, res) => {
	taskController.deleteTask(req.params.id).then(
		resultFromController => res.send(resultFromController));
})

// router to update a task
router.put('/:id', (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


// To export the router object to be used in the 'index.js'
module.exports = router;
